# local environment (le)
[![pipeline status](https://gitlab.com/pgmtc/le/badges/master/pipeline.svg)](https://gitlab.com/pgmtc/le/commits/master)
[![coverage report](https://gitlab.com/pgmtc/le/badges/master/coverage.svg)](https://gitlab.com/pgmtc/le/commits/master)

## Prerequisites
1. Running docker daemon

## Installation
### macOS
* Add pgmtc homebrew repository
```
brew tap pgmtc/repo
brew update
```
* Install le tool
```
brew install pgmtc/repo/le
```
* Init config
```
le config init
```

### Manual
1. Download appropriate package from [releases page](https://gitlab.com/pgmtc/le/releases)
2. Unzip it, there should be an executable inside
3. Put it somewhere to path, potentially chmod a+x it
4. Run it from the terminal / command line

## Usage 
Syntax:

`le [module] [action] parameters`

In cases related to containers (vast majority), syntax is as follows:

- `le [module] [action] component` : runs action for component
- `le [module] [action] component1 component2 ... componentN` : runs for component1 .. componentN
- `le [module] [action] all` : runs for all available components


## Modules
### local
Local module is responsible for running local environments
It has the following actions

`le local status`: prints status of the local environment

`le local pull [component]`: used for components with remote docker images

`le local create [component]`: create a docker container for the component

`le local remove [component]`: removes docker container of the component

`le local start [component]`: starts docker container for the component

`le local stop [component]`: stops the docker container for the component

`le local logs [component]`: shows logs of the related docker container

`le local watch [component]`: shows logs on the 'follow' basis

### builder
`le builder build [component]`: builds a docker image for the component

Build definition is stored in .builder directory inside the project.
It can be override by providing --specdir argument.
To create empty build definition directory, run 
`le builder init`


### config
Config is a centralized storage used by other modules.

`le config init`: Run after the installation. Creates ~/.le, config file and default profile

`le config status'`: Prints out information about the current profile. Adding -v makes it more verbose

`le config create [profile] [source-profile]`: Creates a new profile. By passing source-profile parameter (not mandatory), it uses it as a base for copy

`le config switch [profile]`: Switches current profile to another one

### agent
Very much work in progress
#### Generate Certificates
```
openssl genrsa -out ~/.le/le.key 2048
openssl req -new -x509 -sha256 -key ~/.le/le.key -out ~/.le/le.crt -days 3650
openssl req -new -sha256 -key ~/.le/le.key -out ~/.le/le.csr
openssl x509 -req -sha256 -in ~/.le/le.csr -signkey ~/.le/le.key -out ~/.le/le.crt -days 3650
```