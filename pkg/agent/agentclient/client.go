package agentclient

import (
	"context"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"gitlab.com/pgmtc/le/pkg/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io"
	"os"
	"path/filepath"
	"time"
)

// AgentClient represents Agent Interface
type AgentClient interface {
	Connect() error
	Disconnect() error
	Ping() error
	SendFile(filePath string) error
	Execute() error
}

type agentClient struct {
	serverHost  string
	conn        *grpc.ClientConn
	ctx         common.Context
	agentClient api.LeAgentClient
}

// NewAgentClient is a constructor
func NewAgentClient(serverHost string, ctx common.Context) AgentClient {
	return &agentClient{
		serverHost: serverHost,
		ctx:        ctx,
	}
}

// Connects connects to remote server
func (c *agentClient) Connect() error {
	var err error
	certFile := common.ParsePath(fmt.Sprintf("~/.le/leagent.crt"))
	creds, err := credentials.NewClientTLSFromFile(certFile, "")
	if err != nil {
		return fmt.Errorf("could not load TLS keys: %s", err)
	}
	//if c.conn, err = grpc.Dial(c.serverHost, grpc.WithBlock(), grpc.WithTransportCredentials(creds)); err != nil {
	if c.conn, err = grpc.Dial(c.serverHost, grpc.WithTransportCredentials(creds)); err != nil {
		return fmt.Errorf("error when connecting to the remote agent: %s", err)
	}
	c.agentClient = api.NewLeAgentClient(c.conn)
	return nil
}

// Disconnect disconnects from remote server
func (c *agentClient) Disconnect() error {
	if c.conn != nil {
		return c.conn.Close()
	}
	return nil
}

// Ping sends echo to the server
func (c *agentClient) Ping() error {
	if c.conn == nil {
		return fmt.Errorf("connection is nil, run Connect() first")
	}

	response, err := c.agentClient.Ping(context.Background(), &api.PingMessage{Message: "ping"})
	if err != nil {
		return fmt.Errorf("error when calling server: %s", err.Error())
	}
	c.ctx.Log.Infof("Response from server: %s\n", response.Message)
	return nil
}

func (c *agentClient) Execute() error {
	c.ctx.Log.Infof("\n")
	stream, err := c.agentClient.Execute(context.Background())
	if err != nil {
		return err
	}
	done := make(chan struct{})
	exitAfterResponse := false
	// First go routine sends commands to the server
	go func() {
		for {
			if err = stream.Send(&api.ExecuteRequest{Module: "module", Action: "action"}); err != nil {
				c.ctx.Log.Errorf("error when sending command to stream: %s\n", err.Error())
				return
			}
			time.Sleep(1 * time.Second)
			if err = stream.Send(&api.ExecuteRequest{Module: "module", Action: "action2"}); err != nil {
				c.ctx.Log.Errorf("error when sending command to stream: %s\n", err.Error())
				return
			}
			exitAfterResponse = true
			return
		}
	}()
	// Second go routine receives responses from the server
	go func() {
		for {
			resp, err := stream.Recv()
			if err != nil {
				c.ctx.Log.Errorf("error when receiving message from stream: %s\n", err.Error())
				return
			}
			c.ctx.Log.Infof("server response: %s\n", resp.Message)
			if exitAfterResponse {
				done <- struct{}{}
				return
			}
		}
	}()
	<-done
	return nil
}

// SendFile sends a local file to the remote server
func (c *agentClient) SendFile(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	fi, err := file.Stat()
	if err != nil {
		return err
	}

	stream, err := c.agentClient.Upload(context.Background())
	if err != nil {
		return err
	}

	chunkSize := 1024
	sent := 0
	buf := make([]byte, chunkSize)
	for {
		n, err := file.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if err = stream.Send(&api.Chunk{Content: buf[:n], FileName: filepath.Base(filePath)}); err != nil {
			return err
		}
		sent += chunkSize
		c.ctx.Log.Infof("\r%.2f pct", 100*float64(sent)/float64(fi.Size()))
	}
	status, err := stream.CloseAndRecv()
	if err != nil {
		return err
	}

	c.ctx.Log.Infof("\rSuccessfully sent %s. Remote location: %s\n", filePath, status.Location)
	return nil
}
