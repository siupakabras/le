package agentserver

import (
	"fmt"
	"github.com/apex/log"
	"github.com/apex/log/handlers/text"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"gitlab.com/pgmtc/le/pkg/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
)

func init() {
	log.SetHandler(text.Default)
	log.SetLevel(log.DebugLevel)
}

// AgentServer represents the gRPC server
type AgentServer struct {
	ctx common.Context
}

// NewAgentServer is a constructor
func NewAgentServer(port string, ctx common.Context) error {
	// create a listener on TCP port 7777
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		return fmt.Errorf("failed to listen: %s", err.Error())
	}
	// create a server instance
	s := AgentServer{ctx: ctx}
	// create a gRPC server object
	certFile := common.ParsePath("~/.le/leagent.crt")
	keyFile := common.ParsePath("~/.le/leagent.key")
	creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
	if err != nil {
		log.Fatalf("could not load TLS keys: %s", err)
	}

	grpcServer := grpc.NewServer(grpc.Creds(creds))
	// attach the Ping service to the server
	api.RegisterLeAgentServer(grpcServer, &s)
	// start the server
	ctx.Log.Infof("Agent server starting on :%s\n", port)
	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to server: %s", err.Error())
	}
	return nil
}
