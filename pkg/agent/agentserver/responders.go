package agentserver

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Ping generates response to a Ping request
func (s *AgentServer) Ping(ctx context.Context, in *api.PingMessage) (*api.PingMessage, error) {
	s.ctx.Log.Infof("Received message %s, responding with pong\n", in.Message)
	return &api.PingMessage{Message: "pong"}, nil
}

// Upload is a responder for upload file
func (s *AgentServer) Upload(stream api.LeAgent_UploadServer) error {
	tempDir, err := ioutil.TempDir("", "le-uploads")
	if err != nil {
		return err
	}
	tempFile, err := ioutil.TempFile(os.TempDir(), "")
	if err != nil {
		fmt.Println("Cannot create temporary file", err)
	}

	received := 0
	fileName := ""
	for {
		chunk, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// done with the transfer
				break
			}
			err = errors.Wrapf(err, "failed unexpectedly while reading chunks from stream")
			return err
		}
		fileName = chunk.FileName
		received += len(chunk.Content)

		s.ctx.Log.Infof("Receiving file, received %d bytes\r", received)
		_, err = tempFile.Write(chunk.Content)
		if err != nil {
			return errors.Wrapf(err, "error when writing file")
		}
	}
	err = tempFile.Close()
	if err != nil {
		return errors.Wrap(err, "error when closing the file")
	}
	// Rename file to original file name
	finalFile := filepath.Join(tempDir, fileName)
	if err = os.Rename(tempFile.Name(), finalFile); err != nil {
		return errors.Wrapf(err, "error when renaming file %s to %s", tempFile.Name(), finalFile)
	}

	s.ctx.Log.Infof("Finished receiving, file saved to %s\n", finalFile)

	return stream.SendAndClose(&api.UploadStatus{
		Location: finalFile,
		Code:     api.UploadStatusCode_Ok,
	})
}

// Execute is a responder for execute command
func (s *AgentServer) Execute(stream api.LeAgent_ExecuteServer) error {
	ctx := stream.Context()
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		req, err := stream.Recv()
		if err == io.EOF {
			/// return will close stream from server side
			s.ctx.Log.Infof("exiting")
			return nil
		}
		if err != nil {
			return fmt.Errorf("error when receiving stream: %s", err.Error())
		}
		s.ctx.Log.Infof("received cmd: %s -> %s\n", req.Action, req.Module)
		resp := api.ExecuteResponse{
			Message: fmt.Sprintf("response for %s -> %s", req.Action, req.Module),
		}
		err = stream.Send(&resp)
		if err != nil {
			s.ctx.Log.Errorf("error when sending response: %s", err.Error())
		}
	}
}
