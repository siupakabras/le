package agent

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/agentclient"
	"gitlab.com/pgmtc/le/pkg/agent/agentserver"
	"gitlab.com/pgmtc/le/pkg/agent/cert"
	"gitlab.com/pgmtc/le/pkg/common"
	"io/ioutil"
	"os"
	"strings"
)

func startAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.Context, args ...string) error {
			var port = "7777"
			portArgPresent, ports := common.ParseCmdArgument("port", args...)
			if portArgPresent && len(ports) > 0 {
				port = ports[0]
			}
			return agentserver.NewAgentServer(port, ctx)
		},
	}
}

func generateCertificateAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.Context, args ...string) error {
			_, hosts := common.ParseCmdArgument("host", args...)
			if len(hosts) == 0 {
				ctx.Log.Infof("No --host argument provided, generating for localhost, 127.0.0.1\n")
				hosts = []string{"localhost", "127.0.0.1"}
			} else {
				ctx.Log.Infof("generating for the following hosts: %v\n", strings.Join(hosts, ", "))
			}
			key, cert, err := cert.Generate(ctx.Log, hosts)
			if err != nil {
				return err
			}

			// Check that files don't exist already
			force, _ := common.ParseCmdArgument("force", args...)
			certPath := common.ParsePath("~/.le/leagent.crt")
			keyPath := common.ParsePath("~/.le/leagent.key")

			if !force {
				if _, err := os.Stat(certPath); !os.IsNotExist(err) {
					return fmt.Errorf("file %s already exist, add --force to owerwrite", certPath)
				}
				if _, err := os.Stat(keyPath); !os.IsNotExist(err) {
					return fmt.Errorf("file %s already exist, add --force to owerwrite", keyPath)
				}
			}
			// Write Cert File
			if writeErr := ioutil.WriteFile(certPath, []byte(cert), 644); writeErr != nil {
				return fmt.Errorf("error writing file: %s", certPath)
			}
			ctx.Log.Infof("Cert file saved : %s (copy this file over to your remote client)\n", certPath)
			// Write Key File
			if writeErr := ioutil.WriteFile(keyPath, []byte(key), 644); writeErr != nil {
				return fmt.Errorf("error writing file: %s", certPath)
			}
			ctx.Log.Infof("Key file saved  : %s (store this only on your server)\n", keyPath)
			return nil
		},
	}
}

func remoteAgentAction(handler func(client agentclient.AgentClient, args ...string) error) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.Context, args ...string) error {
			agentHost := ctx.Config.Config().RemoteAgentHost
			if agentHost == "" {
				return fmt.Errorf("agenthost config property is missing. Edit your config")
			}
			client := agentclient.NewAgentClient(agentHost, ctx)
			if err := client.Connect(); err != nil {
				return fmt.Errorf(err.Error())
			}
			defer client.Disconnect()
			return handler(client, args...)
		},
	}
}

func remotePingAction(client agentclient.AgentClient, args ...string) error {
	return client.Ping()
}

func remoteCopyAction(client agentclient.AgentClient, args ...string) error {
	if len(args) == 0 {
		return fmt.Errorf("missing file parameter")
	}
	filePath := args[0]
	return client.SendFile(filePath)
}

func remoteCommandAction(client agentclient.AgentClient, args ...string) error {
	return client.Execute()
}
