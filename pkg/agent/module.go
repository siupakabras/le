package agent

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

// Module represents a module
type Module struct{}

// GetActions return list of actions for a module
func (Module) GetActions() map[string]common.Action {
	return map[string]common.Action{
		"start":                startAction(),
		"ping":                 remoteAgentAction(remotePingAction),
		"copy":                 remoteAgentAction(remoteCopyAction),
		"command":              remoteAgentAction(remoteCommandAction),
		"generate-certificate": generateCertificateAction(),
	}
}
