package builder

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

//go:generate mockgen -destination=./mocks/mock_builder.go -package=mocks gitlab.com/pgmtc/le/pkg/builder Builder

// Builder represents interface for builder
type Builder interface {
	BuildImage(ctx common.Context, image string, buildRoot string, dockerFile string, buildArgs []string, noCache bool) error
	BuildFile(ctx common.Context, file string, buildRoot string, builderImage string, builderCmd string, noCache bool) error
}
