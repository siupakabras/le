package builder

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path"
)

func getBuildAction(builder Builder) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.Context, args ...string) error {
			noCache := false
			specDir := builderDir
			tag := "latest"

			for idx, arg := range args {
				if arg == "--nocache" {
					noCache = true
				}
				if arg == "--specdir" {
					if len(args) <= idx+1 {
						return fmt.Errorf("missing parameter for --specdir")
					}
					specDir = args[idx+1]
					ctx.Log.Debugf("Using %s as build spec dir\n", specDir)
				}
				if arg == "--tag" {
					if len(args) <= idx+1 {
						return fmt.Errorf("missing parameter for --tag")
					}
					tag = args[idx+1]
					ctx.Log.Debugf("Using version tag %s\n", tag)
				}
			}
			image, buildRoot, dockerFile, buildArgs, file, builderImage, builderCmd, err := parseBuildProperties(specDir)
			if err != nil {
				return err
			}
			if image != "" && file != "" {
				return fmt.Errorf("config must have either image OR file property, not both")
			}
			if image != "" {
				return builder.BuildImage(ctx, image+":"+tag, buildRoot, dockerFile, buildArgs, noCache)
			}
			if file != "" {
				ctx.Log.Debugf("want to build a file: %s, %s, %s\n", file, builderImage, builderCmd)
				return builder.BuildFile(ctx, file, buildRoot, builderImage, builderCmd, noCache)
			}
			return fmt.Errorf("config must have either image or file set")
		},
	}
}

func parseBuildProperties(builderDir string) (image string, buildRoot string, dockerFile string, buildArgs []string, file string, builderImage string, builderCmd string, resultErr error) {
	// Try to read builder config
	configDirPath := common.ParsePath(builderDir)
	if _, err := os.Stat(configDirPath); os.IsNotExist(err) {
		resultErr = errors.Errorf("Unable to determine build configuration: %s", err.Error())
		return
	}

	bcnf := buildConfig{}
	bcnfPath := path.Join(builderDir, configFileName)
	err := common.YamlUnmarshall(bcnfPath, &bcnf)
	if err != nil {
		resultErr = errors.Errorf("Unable to parse config file %s: %s", bcnfPath, err.Error())
		return
	}
	image = bcnf.Image
	buildRoot = common.ParsePath(bcnf.BuildRoot)
	dockerFile = common.ParsePath(bcnf.Dockerfile)
	buildArgs = bcnf.BuildArgs
	file = bcnf.File
	builderImage = bcnf.BuilderImage
	builderCmd = bcnf.BuilderCmd
	return
}
