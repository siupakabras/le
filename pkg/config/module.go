package config

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

// Module represents a module
type Module struct{}

// GetActions returns list of actions for this module
func (Module) GetActions() map[string]common.Action {
	return map[string]common.Action{
		"default": &statusAction,
		"status":  &statusAction,
		"init":    &initAction,
		"create":  &createAction,
		"switch":  &switchAction,
	}
}
