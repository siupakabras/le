package docker

import (
	"gitlab.com/pgmtc/le/pkg/common"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

func isResponding(cmp common.Component) (result string, resultErr error) {
	timeout := time.Duration(3 * time.Second)
	if cmp.TestURL == "" {
		result = ""
		return
	}

	client := &http.Client{
		Timeout: timeout,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	resp, err := client.Get(cmp.TestURL)
	if err != nil {
		result = "ERR"
		resultErr = err
		return
	}
	defer resp.Body.Close()
	result = strconv.Itoa(resp.StatusCode)
	return
}

func downloadFile(url string, filePath string) (resultErr error) {
	timeout := time.Duration(3 * time.Second)
	client := &http.Client{
		Timeout: timeout,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	resp, resultErr := client.Get(url)
	if resultErr != nil {
		return
	}
	defer resp.Body.Close()

	// Create the file
	out, resultErr := os.Create(filePath)
	if resultErr != nil {
		return
	}
	defer out.Close()

	// Write the body to file
	_, resultErr = io.Copy(out, resp.Body)
	return
}
