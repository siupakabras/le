package common

import "os"

//go:generate mockgen -destination=./mocks/mock_fshandler.go -package=mocks gitlab.com/pgmtc/le/pkg/common FSHandler

// FSHandler is an abstraction for file system
type FSHandler interface {
	MkdirAll(path string, perm os.FileMode) error
	Stat(name string) (os.FileInfo, error)
	Create(name string) (*os.File, error)
}

// OsFileSystemHandler is an OS file system handler
type OsFileSystemHandler struct{}

// Stat returns os.stat
func (OsFileSystemHandler) Stat(name string) (os.FileInfo, error) {
	return os.Stat(name)
}

// Create creates a file
func (OsFileSystemHandler) Create(name string) (*os.File, error) {
	return os.Create(name)
}

// MkdirAll makes directory
func (OsFileSystemHandler) MkdirAll(path string, perm os.FileMode) error {
	return os.MkdirAll(path, perm)
}
