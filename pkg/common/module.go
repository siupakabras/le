package common

// Module represents an interface for modules
type Module interface {

	// GetActions returns map of actions for the module
	GetActions() map[string]Action
}

// Logger is an interface representing Logger
type Logger interface {
	Errorf(format string, a ...interface{})
	Debugf(format string, a ...interface{})
	Infof(format string, a ...interface{})
	Write(p []byte) (n int, err error)
}

// Context is a context
type Context struct {
	Log    Logger
	Config ConfigProvider
	Module Module
}

// Action represents Action
type Action interface {
	Run(ctx Context, args ...string) error
}

// RawActionHandler represents RawActionHandler
type RawActionHandler func(ctx Context, args ...string) error

// RawAction represents raw action
type RawAction struct {
	Handler RawActionHandler
}

// Run runs the action
func (a *RawAction) Run(ctx Context, args ...string) error {
	return a.Handler(ctx, args...)
}
