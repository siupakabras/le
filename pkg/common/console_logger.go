package common

import (
	"fmt"
	"github.com/fatih/color"
)

// ConsoleLogger is a struct representing console logger
type ConsoleLogger struct{}

// Errorf prints an error
func (ConsoleLogger) Errorf(format string, a ...interface{}) {
	fmt.Printf(color.HiRedString(format, a...))
}

// Debugf prints a debug message
func (ConsoleLogger) Debugf(format string, a ...interface{}) {
	fmt.Printf(color.WhiteString(format, a...))
}

// Infof prints an info message
func (ConsoleLogger) Infof(format string, a ...interface{}) {
	fmt.Printf(color.HiWhiteString(format, a...))
}

// Write is a basic writter to the log
func (ConsoleLogger) Write(p []byte) (n int, err error) {
	return fmt.Printf("%s", p)
}
